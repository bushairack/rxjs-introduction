import { of } from "rxjs";

const observable = of("abc", "def", "ghi");

const observer = {
  next: (result) => {
    console.log(result);
  },
  complete: () => {
    console.log("completed");
  },
};

observable.subscribe(observer);
